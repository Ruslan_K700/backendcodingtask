﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendCodingTask.Models;

namespace BackendCodingTask.Controllers
{
    [Route("api/[controller]")]
    public class CharacterController : Controller
    {
        public CharacterController(IRepository characters)
        {
            Characters = characters;
        }
        public IRepository Characters { get; set; }

        [HttpGet("Create")]
        public IActionResult CreateNewCharacter(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                Character character = Characters.CreateNewCharacter(name);
                return new ObjectResult(character);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet("GetCharacter")]
        public IActionResult GetCharacter(int id)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            return new ObjectResult(character);
        }

        [HttpGet("GetAvailableItems")]
        public IActionResult GetAvailableItems(int id)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            return new ObjectResult(Characters.GetAvailableItems(character));
        }

        [HttpGet("BuyItem")]
        public IActionResult BuyItem(int id, int itemId)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            var item = Characters.FindItem(itemId);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(Characters.BuyItem(character, item));
        }

        [HttpGet("SellItem")]
        public IActionResult SellItem(int id, int itemId)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            var item = Characters.FindItem(itemId);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(Characters.SellItem(character, item));
        }

        [HttpGet("Reward")]
        public IActionResult Reward(int id, int xp, int gold)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            return new ObjectResult(Characters.Reward(character, xp, gold));
        }

        [HttpGet("IncreaseStat")]
        public IActionResult IncreaseStat(int id, Stats stat)
        {
            var character = Characters.FindCharacter(id);
            if (character == null)
            {
                return NotFound();
            }
            return new ObjectResult(Characters.IncreaseStat(character, stat));
        }
    }
}
