﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendCodingTask.Models
{
    public enum Stats
    {
        Strength,
        Agility,
    }

    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gold { get; set; }
        public int Level { get; set; }
        public int Xp { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }
        public int AvailableStatPoints { get; set; }
        public int InventorySlots { get; set; }
        public List<Item> Items { get; set; }
    }
}
