﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendCodingTask.Models
{
    public class Experience
    {
        public const int maxLelel = 10;

        public Dictionary<int, int> Table
        {
            get { return table; }
        }
        Dictionary<int, int> table;

        public Experience()
        {
            table = new Dictionary<int, int>();
            int x = 50;
            int y = 150;
            for (int i = 1; i < maxLelel; i++)
	        {
                x += y;
                table[i] = x;
                y += 100;
            }
        }
    }
}
