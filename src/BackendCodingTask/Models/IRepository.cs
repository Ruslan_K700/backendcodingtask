﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendCodingTask.Models
{
    public interface IRepository
    {
        IEnumerable<Item> GetAllItems();
        Item AddItem(Item item);
        Item FindItem(int Id);
        Item RemoveItem(int Id);
        void UpdateItem(Item item);

        IEnumerable<Character> GetAllCharacters();
        Character AddCharacter(Character character);
        Character FindCharacter(int Id);
        Character RemoveCharacter(int Id);
        void UpdateCharacter(Character item);

        Character CreateNewCharacter(string name);
        IEnumerable<Item> GetAvailableItems(Character character);
        bool BuyItem(Character character, Item item);
        bool SellItem(Character character, Item item);
        Character Reward(Character character, int xp, int gold);
        bool IncreaseStat(Character character, Stats stat);
    }
}
