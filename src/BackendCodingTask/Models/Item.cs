﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendCodingTask.Models
{
    public enum ItemCategory
    {
        Armor,
        Weapon,
    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ItemCategory Category { get; set; }
        public int Cost { get; set; }
        public float Weight { get; set; }
        public int StrengthRequirement { get; set; }
        public int AgilityRequirement { get; set; }
    }
}
