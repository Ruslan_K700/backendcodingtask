﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendCodingTask.Models
{
    public class Repository : IRepository
    {
        private static ConcurrentDictionary<int, Item> _items =
              new ConcurrentDictionary<int, Item>();

        private static ConcurrentDictionary<int, Character> _characters =
              new ConcurrentDictionary<int, Character>();

        private static Experience _experience = new Experience();

        public Repository()
        {
            AddItem(new Item
            {
                Name = "Tunic with vest",
                Category = ItemCategory.Armor,
                Cost = 50,
                Weight = 1f,
                StrengthRequirement = 1,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Linen Tunic",
                Category = ItemCategory.Armor,
                Cost = 100,
                Weight = 1f,
                StrengthRequirement = 1,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Coat",
                Category = ItemCategory.Armor,
                Cost = 200,
                Weight = 1.5f,
                StrengthRequirement = 2,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Tabard",
                Category = ItemCategory.Armor,
                Cost = 300,
                Weight = 2f,
                StrengthRequirement = 3,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Aketon",
                Category = ItemCategory.Armor,
                Cost = 500,
                Weight = 3f,
                StrengthRequirement = 4,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Spiked Club",
                Category = ItemCategory.Weapon,
                Cost = 145,
                Weight = 1f,
                StrengthRequirement = 1,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Old Axe",
                Category = ItemCategory.Weapon,
                Cost = 200,
                Weight = 1f,
                StrengthRequirement = 1,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Rusty Pick",
                Category = ItemCategory.Weapon,
                Cost = 230,
                Weight = 1f,
                StrengthRequirement = 1,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Hunting Bow",
                Category = ItemCategory.Weapon,
                Cost = 300,
                Weight = 1f,
                StrengthRequirement = 0,
                AgilityRequirement = 1
            });
            AddItem(new Item
            {
                Name = "Short Sword",
                Category = ItemCategory.Weapon,
                Cost = 400,
                Weight = 1.5f,
                StrengthRequirement = 2,
                AgilityRequirement = 0
            });
            AddItem(new Item
            {
                Name = "Short Bow",
                Category = ItemCategory.Weapon,
                Cost = 500,
                Weight = 1.5f,
                StrengthRequirement = 0,
                AgilityRequirement = 2
            });

            CreateNewCharacter("Test Character");

        }

        public IEnumerable<Item> GetAllItems()
        {
            return _items.Values;
        }

        public Item AddItem(Item item)
        {
            item.Id = _items.Count();
            _items[item.Id] = item;
            return item;
        }

        public Item FindItem(int id)
        {
            Item item;
            _items.TryGetValue(id, out item);
            return item;
        }

        public Item RemoveItem(int id)
        {
            Item item;
            _items.TryRemove(id, out item);
            return item;
        }

        public void UpdateItem(Item item)
        {
            _items[item.Id] = item;
        }

        public IEnumerable<Character> GetAllCharacters()
        {
            return _characters.Values;
        }

        public Character AddCharacter(Character character)
        {
            character.Id = _characters.Count();
            _characters[character.Id] = character;
            return character;
        }

        public Character FindCharacter(int id)
        {
            Character character;
            _characters.TryGetValue(id, out character);
            return character;
        }

        public Character RemoveCharacter(int id)
        {
            Character character;
            _characters.TryRemove(id, out character);
            return character;
        }

        public void UpdateCharacter(Character character)
        {
            _characters[character.Id] = character;
        }

        public Character CreateNewCharacter(string name)
        {
            var query =
                from item in _items
                where item.Value.Cost <= 100 && item.Value.Category == ItemCategory.Armor
                select item.Value;

            List<Item> lowCostArmors = query.ToList();

            query =
                from item in _items
                where item.Value.Cost <= 300 && item.Value.Category == ItemCategory.Weapon
                select item.Value;

            List<Item> lowCostWeapons = query.ToList();

            List<Item> items = new List<Item>();
            Random rnd = new Random();

            int randomInt = rnd.Next(0, lowCostArmors.Count());
            items.Add(lowCostArmors[randomInt]);

            randomInt = rnd.Next(0, lowCostWeapons.Count());
            items.Add(lowCostWeapons[randomInt]);

            Character character = AddCharacter(new Character
            {
                Name = name,
                Gold = 100,
                Level = 1,
                Strength = 1,
                Agility = 1,
                InventorySlots = 2,
                Items = items
            });

            return character;
        }

        public bool ItemisAvailable(Character character, Item item)
        {
            if (!character.Items.Contains(item)
                && character.Items.Count() < character.InventorySlots
                && GetCarryingWeight(character) + item.Weight <= character.Strength * 10
                && item.Cost <= character.Gold
                && item.StrengthRequirement <= character.Strength
                && item.AgilityRequirement <= character.Agility)
                return true;
            return false;
        }

        public float GetCarryingWeight(Character character)
        {
            float carryingWeight = 0f;
            foreach (Item item in character.Items)
            {
                carryingWeight += item.Weight;
            }
            return carryingWeight;
        }

        public IEnumerable<Item> GetAvailableItems(Character character)
        {
            List<Item> AvailableItems = new List<Item>();
            foreach (var pair in _items)
            {
                if (ItemisAvailable(character, pair.Value)) AvailableItems.Add(pair.Value);
            }
            return AvailableItems;
        }

        public bool BuyItem(Character character, Item item)
        {
            if (ItemisAvailable(character, item))
            {
                character.Gold -= item.Cost;
                character.Items.Add(item);
                return true;
            }
            return false;
        }

        public bool SellItem(Character character, Item item)
        {
            if (character.Items.Contains(item))
            {
                character.Gold += item.Cost / 2;
                character.Items.Remove(item);
                return true;
            }
            return false;
        }

        public Character Reward(Character character, int xp, int gold)
        {
            if (xp > 0 && character.Level < Experience.maxLelel)
            {
                character.Xp = Math.Min(character.Xp + xp, _experience.Table[Experience.maxLelel - 1]);
                int level = 0;
                foreach (var pair in _experience.Table)
                {
                    if (pair.Key >= character.Level && pair.Value <= character.Xp) level++;
                }
                LevelUp(character, level);
            }
            if (gold > 0) character.Gold += gold;
            return character;
        }

        public void LevelUp(Character character, int level)
        {
            character.Level += level;
            character.Gold += level * 100;
            character.InventorySlots += level;
            character.AvailableStatPoints += level;
        }

        public bool IncreaseStat(Character character, Stats stat)
        {
            if(character.AvailableStatPoints > 0)
            {
                if (stat == Stats.Agility)
                    character.Agility++;
                else
                    character.Strength++;
                character.AvailableStatPoints--;
                return true;
            }
            return false;
        }
    }
}
